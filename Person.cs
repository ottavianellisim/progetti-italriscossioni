﻿using System;
using System.Text.RegularExpressions;
namespace Italriscossioni
{
    public class Person
    {
        public static void Check(string name, string surname, string address, string city, string cap, string province, string nation, string mail, string cf, string iban, string phoneNumber, string homeNumber)
        {
            if (!CheckName(name) ||
                !CheckSurname(surname) ||
                !CheckAddress(address) ||
                !CheckCity(city) ||
                !CheckCAP(cap) ||
                !CheckProvince(province) ||
                !CheckNation(nation) ||
                !CheckEmail(mail) ||
                !CheckCF(cf) ||
                !CheckIBAN(iban) ||
                !CheckPhoneNumber(phoneNumber) ||
                !CheckNumberHome(homeNumber))
            {
                Console.WriteLine("ESITO NEGATIVO");
            }
            else
            {
                Console.WriteLine("ESITO POSITIVO!\n");
                Console.WriteLine(@"RIEPOLOGO:

Nome: {0}
Cognome: {1}
Codice Fiscale: {2}
Indirizzo: {3}
Città: {4}
Provincia {5}
CAP: {6}
Codice Nazione: {7}
Email: {8}
Numero di cellulare: {9}
Numero fisso: {10}
IBAN: {11}

                ", name, surname, cf.ToUpper(), address, city, province.ToUpper(), cap, nation.ToUpper(), mail, phoneNumber, homeNumber, iban.ToUpper());
            }
        }

        public static bool CheckTemplate(string reg, string value, string errorStr)
        {
            var regex = reg;
            var mc = Regex.IsMatch(value, regex);
            if (!mc)
            {
                Console.WriteLine(errorStr);
            }
            return mc;

        }

        public static bool CheckName(string name)
        {
            return CheckTemplate(@"^[A-Za-z]+$", name, "Il nome deve essere composto da sole lettere e non essere vuoto\n");
        }

        public static bool CheckSurname(string surname)
        {
            return CheckTemplate(@"^[A-Za-z]+$", surname, "Il cognome deve essere composto da sole lettere e non esser vuoto\n");
        }

        public static bool CheckAddress(string address)
        {
            return CheckTemplate(@"^[A-Za-z]", address, "L'indirizzo non può iniziare con un numero o carattere speciale o esser vuoto\n");
        }

        public static bool CheckCity(string city)
        {
            return CheckTemplate(@"^[A-Za-z]+$", city, "La città deve essere composta da sole lettere e non esser vuoto\n");
        }

        public static bool CheckCAP(string cap)
        {
            return CheckTemplate(@"[0-9]{5}", cap, "Il CAP deve essere composto da 5 cifre e non essere vuoto\n");
        }

        public static bool CheckProvince(string province)
        {
            return CheckTemplate(@"^[A-Za-z]{2}$", province, "Il codice della provincia deve essere composto da 2 caratteri alfabetici e non esser vuoto\n");
        }

        public static bool CheckNation(string nation)
        {
            return CheckTemplate(@"^[A-Za-z]{2}$", nation, "Il codice della nazione deve essere composto da 2 caratteri alfabetici in maiuscolo e non esser vuoto\n");
        }

        public static bool CheckEmail(string mail)
        {
            return CheckTemplate(@"([A-Za-z]+[-_]*[0-9]*)@{1}.([a-z]+).[a-z]{2,}", mail, "L'indirizzo mail deve essere composto dalla local part precedente al @ e dalla domain part dopoo @ e non esser vuoto\n");
        }

        public static bool CheckCF(string cf)
        {
            return CheckTemplate(@"^[A-Za-z]{2}[A-Za-z]{4}[0-9]{2}[A-Za-z]{1}[0-9]{2}[A-Za-z]{1}[0-9]{3}[A-Za-z]{1}$", cf, @"Il codice fiscale deve essere non vuoto e composto da:

3 caratteri alfabetici per il cognome + 3 caratteri alfabetici per il nome + 2 cifre per l'anno di nascita + 1 carattere per il mese + 2 cifre per il giorno di nascita e il sesso + 4 caratteri per il comune di nascita o il paese estero di nascita + 1 carattere alfabetico come codice di controllo
");
        }

        public static bool CheckIBAN(string iban)
        {
            return CheckTemplate(@"^IT[0-9]{2}[A-Za-z]{1}[0-9]{5}[0-9]{5}[0-9]{12}$", iban, @"L'IBAN deve essere non vuoto e composto da:

IT + 2 cifre + 1 cifra + 5 cifre + 5 cifre + 12 cifre
");
        }

        public static bool CheckPhoneNumber(string number)
        {
            return CheckTemplate(@"^[\+]{0,1}[0-9]{0,2}[0-9]{10,12}$", number, "Il numero di cellulare deve essere non vuoto e può essere composto dal prefisso nazione (es: +39) e da 10 alle 12 cifre.\n");
        }

        public static bool CheckNumberHome(string number)
        {
            return CheckTemplate(@"^[0-9]{10}$", number, "Il numero fisso deve essere non vuoto e composto da 10 cifre.\n");
        }
    }
}

