﻿using Italriscossioni;

namespace Progetto_Italriscossioni;
class Program
{
    static void Main(string[] args)
    {

        Console.WriteLine("NOME: ");
        var name = Console.ReadLine();

        Console.WriteLine("\n");

        Console.WriteLine("COGNOME: ");
        var surname = Console.ReadLine();

        Console.WriteLine("\n");

        Console.WriteLine("INDIRIZZO: ");
        var address = Console.ReadLine();

        Console.WriteLine("\n");

        Console.WriteLine("CITTA': ");
        var city = Console.ReadLine();

        Console.WriteLine("\n");

        Console.WriteLine("CAP: ");
        var cap = Console.ReadLine();

        Console.WriteLine("\n");

        Console.WriteLine("SIGLA PROVINCIA: ");
        var province = Console.ReadLine();

        Console.WriteLine("\n");

        Console.WriteLine("SIGLA NAZIONE (2 caratteri): ");
        var nation = Console.ReadLine();

        Console.WriteLine("\n");

        Console.WriteLine("EMAIL: ");
        var mail = Console.ReadLine();

        Console.WriteLine("\n");

        Console.WriteLine("CODICE FISCALE: ");
        var cf = Console.ReadLine();

        Console.WriteLine("\n");

        Console.WriteLine("IBAN (italiano): ");
        var iban = Console.ReadLine();

        Console.WriteLine("\n");

        Console.WriteLine("NUMERO DI CELLULARE: ");
        var phoneNumber = Console.ReadLine();

        Console.WriteLine("\n");

        Console.WriteLine("NUMERO FISSO: ");
        var homeNumber = Console.ReadLine();

        Console.WriteLine("\n");

        Person.Check(name, surname, address, city, cap, province, nation, mail, cf, iban, phoneNumber, homeNumber);
    }
}

